import * as React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  TextField,
  Button
} from '@material-ui/core';

type Props = {
  open: boolean;
  onConfirm: (userName: string, chatName: string) => void;
};

type State = {
  userName: string;
  chatName: string;
};

class ChatForm extends React.Component<Props, State> {
  state: State = {
    userName: '',
    chatName: ''
  };

  constructor(props: Props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  onChange(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState(prevState => ({
      ...prevState,
      [e.target.name]: e.target.value
    }));
  }

  handleClick() {
    const { userName, chatName } = this.state;

    if (userName && chatName) {
      this.props.onConfirm(userName, chatName);
    }
  }

  render() {
    const { open } = this.props;

    return (
      <Dialog open={open} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Chat creation</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To create a chat please enter your user name and chat name
          </DialogContentText>
          <TextField
            name="userName"
            autoFocus
            margin="dense"
            label="User name"
            type="text"
            fullWidth
            onChange={this.onChange}
          />
          <TextField
            name="chatName"
            margin="dense"
            label="Chat name"
            type="text"
            fullWidth
            onChange={this.onChange}
          />
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={this.handleClick}>
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default ChatForm;
