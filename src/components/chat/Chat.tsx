import * as React from 'react';
import { Container } from '@material-ui/core';
import Header from './Header';
import MessageList from './MessageList';
import MessageInput from './MessageInput';
import ChatForm from './ChatForm';
import EditModal from './EditModal';
import { RootState } from '../../store';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import { connect, ConnectedProps } from 'react-redux';
import * as actions from './actions';
import { mapMessages, getUserNumber, getLastTime, sortMessages } from './helpers';

const styles = createStyles({
  root: {
    height: '100vh'
  },
  container: {
    height: '100%',
    display: 'grid',
    gridTemplateRows: '64px calc(100% - 164px) 100px'
  },
  messageList: {
    overflowY: 'auto'
  }
});

const mapStateToProps = (state: RootState) => ({
  userName: state.chat.userName,
  userId: state.chat.userId,
  name: state.chat.name,
  messages: state.chat.messages,
  editModal: state.chat.editModal,
  currentMessageId: state.chat.currentMessageId,
  editedText: state.chat.editedText,
  preloader: state.chat.preloader
});

const mapDispatchToProps = {
  ...actions
};

const connector = connect(mapStateToProps, mapDispatchToProps);

interface Props extends WithStyles<typeof styles>, ConnectedProps<typeof connector> {
  url: string;
}

type State = {
  modalOpened: boolean;
};

class Chat extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      modalOpened: false
    };

    this.confirm = this.confirm.bind(this);
    this.likeMessage = this.likeMessage.bind(this);
    this.addMessage = this.addMessage.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
    this.editMessage = this.editMessage.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onCancelEdit = this.onCancelEdit.bind(this);
    this.onEditedTextChange = this.onEditedTextChange.bind(this);
    this.editLast = this.editLast.bind(this);
  }

  componentDidMount() {
    window.addEventListener('keydown', this.editLast);

    fetch(this.props.url)
      .then(res => res.json())
      .then(result => {
        const messages = result.map(mapMessages);
        this.props.setMessages(messages);
      });
  }

  confirm(userName: string, chatName: string) {
    this.props.initChat(userName, chatName);
    this.setState({ modalOpened: false });
  }

  addMessage(text: string) {
    if (text) this.props.addMessage(text);
  }

  likeMessage(id: string) {
    this.props.likeMessage(id);
  }

  deleteMessage(id: string) {
    this.props.deleteMessage(id);
  }

  editMessage() {
    this.props.editMessage();
    this.props.hideEditModal();
  }

  onEdit(id: string) {
    this.props.setCurrentMessageId(id);
    this.props.updateText(this.props.messages.find(message => message.id === id)?.text);
    this.props.showEditModal();
  }

  onCancelEdit() {
    this.props.hideEditModal();
  }

  onEditedTextChange(text: string) {
    this.props.updateText(text);
  }

  editLast(e: KeyboardEvent) {
    if (e.key !== 'ArrowUp') return;

    const sortedMessages = sortMessages(this.props.messages);
    const lastMessage = sortedMessages[sortedMessages.length - 1];
    if (lastMessage.userId === this.props.userId) {
      this.onEdit(lastMessage.id);
    }
  }

  render() {
    const { userId, name, messages, editModal, editedText, preloader, classes } = this.props;
    const { modalOpened } = this.state;

    return (
      <Container maxWidth="lg" classes={{ root: classes.root }} className="chat">
        <div className={classes.container}>
          <Header
            name={name}
            userCount={getUserNumber(messages)}
            messagesCount={messages.length}
            lastTime={getLastTime(messages)}
          />
          <MessageList
            userId={userId}
            messages={messages}
            isLoaded={preloader}
            onLike={this.likeMessage}
            onDelete={this.deleteMessage}
            onEdit={this.onEdit}
          />
          <MessageInput onAdd={this.addMessage} />
          <EditModal
            open={editModal}
            text={editedText}
            onChange={this.onEditedTextChange}
            onCancel={this.onCancelEdit}
            onEdit={this.editMessage}
          />
          <ChatForm open={modalOpened} onConfirm={this.confirm} />
        </div>
      </Container>
    );
  }
}

export default connector(withStyles(styles)(Chat));
