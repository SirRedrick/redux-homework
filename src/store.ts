import { createStore } from 'redux';
import rootReducer from './rootReducer';
import { ChatState } from './components/chat/reducer';

export interface RootState {
  chat: ChatState;
}

export default createStore(
  rootReducer,
  (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
);
