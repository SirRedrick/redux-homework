import * as React from 'react';
import {
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  DialogActions,
  TextField
} from '@material-ui/core';

type Props = {
  open: boolean;
  text: string | undefined;
  onChange: (text: string) => void;
  onCancel: () => void;
  onEdit: () => void;
};

class EditModal extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(e: React.ChangeEvent<HTMLInputElement>) {
    this.props.onChange(e.target.value);
  }

  render() {
    const { open, text } = this.props;

    return (
      <Dialog
        className={`edit-message-modal ${open ? 'modal-shown' : ''}`}
        open={open}
        aria-labelledby="edit-form-title"
        keepMounted={true}
        disablePortal={true}
      >
        <DialogTitle id="edit-form-title">Edit message text</DialogTitle>
        <DialogContent>
          <DialogContentText>Enter new text for your message</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            label="New text"
            type="text"
            fullWidth
            value={text}
            inputProps={{ className: 'edit-message-input' }}
            onChange={this.onChange}
          />
        </DialogContent>
        <DialogActions>
          <Button className="edit-message-button" color="primary" onClick={this.props.onEdit}>
            Confirm
          </Button>
          <Button className="edit-message-close" color="primary" onClick={this.props.onCancel}>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default EditModal;
