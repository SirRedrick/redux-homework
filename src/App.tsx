import * as React from 'react';
import Chat from './components/chat/Chat';
import CssBaseline from '@material-ui/core/CssBaseline';
import './App.css';

function App() {
  return (
    <React.Fragment>
      <CssBaseline />
      <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />;
    </React.Fragment>
  );
}

export default App;
