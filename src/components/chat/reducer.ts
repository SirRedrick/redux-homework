import { AnyAction } from 'redux';
import { DateTime } from 'luxon';
import { v4 } from 'uuid';
import {
  INIT_CHAT,
  SHOW_EDIT_MODAL,
  HIDE_EDIT_MODAL,
  SET_CURRENT_MESSAGE_ID,
  SET_MESSAGES,
  ADD_MESSAGE,
  LIKE_MESSAGE,
  EDIT_MESSAGE,
  DELETE_MESSAGE,
  UPDATE_TEXT
} from './actionTypes';
import { IMessage } from '../../types/chat';

export interface ChatState {
  userName: string;
  userId: string;
  name: string;
  messages: IMessage[];
  editModal: boolean;
  currentMessageId: string | null;
  editedText: string;
  preloader: boolean;
}

const initialState: ChatState = {
  userName: 'User',
  userId: v4(),
  name: 'Chat',
  messages: [],
  editModal: false,
  currentMessageId: null,
  editedText: '',
  preloader: true
};

const reducer = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case INIT_CHAT:
      return { ...state, name: action.payload.chatName, userName: action.payload.userName };
    case SHOW_EDIT_MODAL:
      return { ...state, editModal: true };
    case HIDE_EDIT_MODAL:
      return { ...state, editModal: false };
    case SET_CURRENT_MESSAGE_ID:
      return { ...state, currentMessageId: action.payload };
    case UPDATE_TEXT:
      return { ...state, editedText: action.payload };
    case SET_MESSAGES:
      return { ...state, messages: action.payload, preloader: false };
    case ADD_MESSAGE:
      return {
        ...state,
        messages: [
          ...state.messages,
          {
            id: v4(),
            user: state.userName,
            userId: state.userId,
            avatar: undefined,
            text: action.payload,
            isLiked: false,
            createdAt: DateTime.now(),
            editedAt: undefined
          }
        ]
      };
    case LIKE_MESSAGE:
      return {
        ...state,
        messages: state.messages.map((message: IMessage) =>
          message.id === action.payload ? { ...message, isLiked: !message.isLiked } : message
        )
      };
    case EDIT_MESSAGE:
      return {
        ...state,
        messages: state.messages.map((message: IMessage) =>
          message.id === state.currentMessageId
            ? { ...message, text: state.editedText, editedAt: DateTime.now() }
            : message
        )
      };
    case DELETE_MESSAGE:
      return {
        ...state,
        messages: state.messages.filter((message: IMessage) => message.id !== action.payload),
        editModal: false
      };
    default:
      return state;
  }
};

export default reducer;
