import { DateTime } from 'luxon';

export interface IMessage {
  id: string;
  userId: string;
  avatar: string | undefined;
  user: string;
  text: string | undefined;
  createdAt: DateTime;
  editedAt: DateTime | null;
  isLiked: boolean;
}
