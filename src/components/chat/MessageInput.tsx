import * as React from 'react';
import { TextField, Button } from '@material-ui/core';

type Props = {
  onAdd: (text: string) => void;
};

type State = {
  text: string;
};
class MessageInput extends React.PureComponent<Props, State> {
  state: State = { text: '' };

  constructor(props: Props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  onChange(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void {
    this.setState({ text: e.currentTarget.value });
  }

  handleClick() {
    this.props.onAdd(this.state.text);
    this.setState({ text: '' });
  }

  render() {
    return (
      <div className="message-input">
        <TextField
          fullWidth
          inputProps={{ className: 'message-input-text' }}
          type="text"
          value={this.state.text}
          onChange={e => this.onChange(e)}
        />
        {
          <Button className="message-input-button" onClick={this.handleClick}>
            Send
          </Button>
        }
      </div>
    );
  }
}

export default MessageInput;
