import * as React from 'react';
import { Toolbar, Typography, Tooltip } from '@material-ui/core';
import { Person as PersonIcon, Chat as ChatIcon } from '@material-ui/icons';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';

const styles = createStyles({
  infoItem: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: '24px'
  },
  infoItemIcon: {
    marginRight: '4px'
  },
  time: {
    marginLeft: 'auto'
  }
});

interface Props extends WithStyles<typeof styles> {
  name: string;
  messagesCount: number;
  userCount: number;
  lastTime: string | null;
}

class Header extends React.Component<Props> {
  render() {
    const { name, messagesCount, userCount, lastTime, classes } = this.props;

    return (
      <Toolbar className="header">
        <Typography variant="h4" component="h1" className="header-title">
          {name}
        </Typography>
        <Typography className={classes.infoItem}>
          <Tooltip title="User count" arrow>
            <PersonIcon className={classes.infoItemIcon} />
          </Tooltip>
          <span className="header-users-count">{userCount}</span>
        </Typography>
        <Typography className={classes.infoItem}>
          <Tooltip title="Message count" arrow>
            <ChatIcon className={classes.infoItemIcon} />
          </Tooltip>
          <span className="header-messages-count">{messagesCount}</span>
        </Typography>
        <Typography className={classes.time}>
          Last message at: <span className="header-last-message-date">{lastTime}</span>
        </Typography>
      </Toolbar>
    );
  }
}

export default withStyles(styles)(Header);
