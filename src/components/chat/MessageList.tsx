import * as React from 'react';
import { List, ListSubheader, Divider, Typography } from '@material-ui/core';
import Message from './Message';
import OwnMessage from './OwnMessage';
import Preloader from '../common/Preloader';
import { IMessage } from '../../types/chat';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import { divideMessagesByDays } from './helpers';

const styles = createStyles({
  listWrapper: {
    overflowY: 'auto'
  },
  ul: {
    padding: 0
  }
});

interface Props extends WithStyles<typeof styles> {
  userId: string;
  messages: IMessage[];
  isLoaded: boolean;
  onLike: (id: string) => void;
  onEdit: (id: string) => void;
  onDelete: (id: string) => void;
}

class MessageList extends React.Component<Props> {
  endRef = React.createRef<HTMLDivElement>();

  componentDidUpdate(prevProps: Props) {
    if (this.props.messages.length !== prevProps.messages.length) {
      this.endRef.current?.scrollIntoView({ behavior: 'smooth' });
    }
  }

  render() {
    const { userId, messages, isLoaded, classes, onLike, onEdit, onDelete } = this.props;
    const dividedMessages = divideMessagesByDays(messages);

    const messageItems = Object.entries(dividedMessages).map(([key, value]) => {
      return (
        <li key={key}>
          <ul className={classes.ul}>
            <ListSubheader className="messages-divider">
              <Divider />
              <Typography>{key}</Typography>
            </ListSubheader>
            {value.map((message: IMessage) => {
              const { id, user, text, createdAt, avatar, isLiked } = message;

              return userId === message.userId ? (
                <OwnMessage
                  key={id}
                  text={text}
                  time={createdAt.toFormat('HH:mm')}
                  onEdit={() => onEdit(id)}
                  onDelete={() => onDelete(id)}
                />
              ) : (
                <Message
                  key={id}
                  name={user}
                  text={text}
                  time={createdAt.toFormat('HH:mm')}
                  avatar={avatar}
                  isLiked={isLiked}
                  onLike={() => onLike(id)}
                />
              );
            })}
          </ul>
        </li>
      );
    });

    return (
      <div className={classes.listWrapper}>
        <List className="message-list" subheader={<li />}>
          {!isLoaded ? messageItems : <Preloader />}
          <div ref={this.endRef} />
        </List>
      </div>
    );
  }
}

export default withStyles(styles)(MessageList);
