import {
  INIT_CHAT,
  SHOW_EDIT_MODAL,
  HIDE_EDIT_MODAL,
  SET_CURRENT_MESSAGE_ID,
  UPDATE_TEXT,
  SET_MESSAGES,
  ADD_MESSAGE,
  LIKE_MESSAGE,
  EDIT_MESSAGE,
  DELETE_MESSAGE
} from './actionTypes';
import { IMessage } from '../../types/chat';

export const initChat = (userName: string, chatName: string) => ({
  type: INIT_CHAT,
  payload: {
    chatName,
    userName
  }
});

export const showEditModal = () => ({
  type: SHOW_EDIT_MODAL
});

export const hideEditModal = () => ({
  type: HIDE_EDIT_MODAL
});

export const setCurrentMessageId = (id: string) => ({
  type: SET_CURRENT_MESSAGE_ID,
  payload: id
});

export const updateText = (text: string | undefined) => ({
  type: UPDATE_TEXT,
  payload: text
});

export const setMessages = (messages: IMessage[]) => ({
  type: SET_MESSAGES,
  payload: messages
});

export const addMessage = (text: string) => ({
  type: ADD_MESSAGE,
  payload: text
});

export const likeMessage = (id: string) => ({
  type: LIKE_MESSAGE,
  payload: id
});

export const editMessage = () => ({
  type: EDIT_MESSAGE
});

export const deleteMessage = (id: string) => ({
  type: DELETE_MESSAGE,
  payload: id
});
