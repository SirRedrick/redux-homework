import { combineReducers } from 'redux';
import chat from './components/chat/reducer';

export const rootReducer = combineReducers({
  chat
});

export default rootReducer;
