import * as React from 'react';
import { ListItem, ListItemText, Grid, IconButton } from '@material-ui/core';
import { Settings as SettingsIcon, Delete as DeleteIcon } from '@material-ui/icons';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';

const styles = createStyles({
  root: {
    width: 'max-content',
    marginLeft: 'auto'
  },
  messageText: {
    flexGrow: 0
  }
});

interface Props extends WithStyles<typeof styles> {
  text: string | undefined;
  time: string;
  onEdit: () => void;
  onDelete: () => void;
}

class OwnMessage extends React.Component<Props> {
  render() {
    const { text, time, classes, onEdit, onDelete } = this.props;

    return (
      <ListItem className="own-message" classes={{ root: classes.root }} alignItems="center">
        <ListItemText
          className={classes.messageText}
          primary={<small className="message-time">{time}</small>}
          secondaryTypographyProps={{ component: 'div' }}
          secondary={
            <Grid container>
              <Grid item xs={12}>
                <span className="message-text">{text}</span>
              </Grid>
              <Grid item xs={12}>
                <IconButton size="small" className="message-edit" onClick={onEdit}>
                  <SettingsIcon fontSize="small" />
                </IconButton>
                <IconButton size="small" className="message-delete" onClick={onDelete}>
                  <DeleteIcon fontSize="small" />
                </IconButton>
              </Grid>
            </Grid>
          }
        />
      </ListItem>
    );
  }
}

export default withStyles(styles)(OwnMessage);
