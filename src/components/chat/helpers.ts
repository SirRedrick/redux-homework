import { DateTime } from 'luxon';
import { IMessage } from '../../types/chat';

type Message = {
  id: string;
  userId: string;
  avatar: string | undefined;
  user: string;
  text: string | undefined;
  createdAt: string;
  editedAt: string;
};

export const mapMessages = (message: Message): IMessage => ({
  ...message,
  isLiked: false,
  createdAt: DateTime.fromISO(message.createdAt, { zone: 'UTC' }),
  editedAt: message.editedAt === '' ? null : DateTime.fromISO(message.editedAt, { zone: 'UTC' })
});

export const getUserNumber = (messages: IMessage[]): number => {
  const userIds = messages.map((message: IMessage) => message.userId);
  return new Set(userIds).size;
};

export const getLastTime = (messages: IMessage[]): string => {
  const dates = messages.map((message: IMessage) => message.createdAt);
  return DateTime.max(...dates)?.toFormat('dd.LL.yyyy HH:mm');
};

export const sortMessages = (messages: IMessage[]): IMessage[] => {
  return [...messages].sort(
    (messageA, messageB) => messageA.createdAt.toMillis() - messageB.createdAt.toMillis()
  );
};

export const divideMessagesByDays = (messages: IMessage[]): Record<string, IMessage[]> => {
  const sortedMessages = sortMessages(messages);

  return sortedMessages.reduce<Record<string, IMessage[]>>((acc, message: IMessage) => {
    const date = message.createdAt.toFormat('cccc, d LLLL');
    const diff = Math.floor(DateTime.now().diff(message.createdAt, ['days']).days);

    switch (diff) {
      case 0:
        return {
          ...acc,
          Today: acc.Today ? [...acc.Today, message] : [message]
        };
      case 1:
        return {
          ...acc,
          Yesterday: acc.Yesterday ? [...acc.Yesterday, message] : [message]
        };
      default:
        return {
          ...acc,
          [date]: acc[date] ? [...acc[date], message] : [message]
        };
    }
  }, {});
};
